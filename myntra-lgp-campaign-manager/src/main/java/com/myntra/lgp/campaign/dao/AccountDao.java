package com.myntra.lgp.campaign.dao;

import org.springframework.data.repository.CrudRepository;

import com.myntra.lgp.campaign.entity.AccountEntity;

public interface AccountDao extends CrudRepository<AccountEntity, Long>{
	
	public AccountEntity findById(Long id);
	
	public AccountEntity findByName(String name);

}
