package com.myntra.lgp.campaign.managers;

import com.myntra.commons.entries.BaseEntry;
import com.myntra.lgp.campaign.client.entry.AdvertiserEntry;

public interface Manager {
	public Object handleUpdate(BaseEntry a);
	
	public Object handleCreate(BaseEntry a);
	
	public Object handleGet(String key);
	
	public Object handleDelete(String key);
}
