package com.myntra.lgp.campaign.entity;

import java.util.Map;
import java.util.TreeMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.myntra.commons.entities.BaseEntity;

@Entity
@Table(name="ad")
public class AdEntity extends BaseEntity {
	
	@Column(name="name")
	private String name;
	
	@Column(name="description")
	private String description;
//	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="campaign_id", updatable=false)
	@NotNull
	private CampaignEntity campaignId;
	
	@Column(name="object_id")
	private String objectId;
	
	@Column(name="budget")
	private Long budget;
	
	@Column(name="boost")
	private Float boost;
	
	@Column(name="persona")
	private String persona;
	
	@Column(name="start_time")
	private Long startTime;
	
	@Column(name="end_time")
	private Long endTime;
	
	@Column(name="cost_per_click")
	private Float costPerClick;
	
	@Column(name="cost_per_view")
	private Float costPerView;
	
	@Column(name="frequency_cap")
	private Integer frequencyCap;
	
	@Column(name="predictive_model")
	private TreeMap<Long,Long> predictiveModel;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
//
	public CampaignEntity getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(CampaignEntity camapignInfo) {
		this.campaignId = camapignInfo;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public Long getBudget() {
		return budget;
	}

	public void setBudget(Long budget) {
		this.budget = budget;
	}

	public String getPersona() {
		return persona;
	}

	public void setPersona(String persona) {
		this.persona = persona;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Float getCostPerClick() {
		return costPerClick;
	}

	public void setCostPerClick(Float costPerClick) {
		this.costPerClick = costPerClick;
	}

	public Float getCostPerView() {
		return costPerView;
	}

	public void setCostPerView(Float costPerView) {
		this.costPerView = costPerView;
	}

	public Integer getFrequencyCap() {
		return frequencyCap;
	}

	public void setFrequencyCap(Integer frequencyCap) {
		this.frequencyCap = frequencyCap;
	}

	

	public Float getBoost() {
		return boost;
	}

	public void setBoost(Float boost) {
		this.boost = boost;
	}

	public TreeMap<Long, Long> getPredictiveModel() {
		return predictiveModel;
	}

	public void setPredictiveModel(TreeMap<Long, Long> predictiveModel) {
		this.predictiveModel = predictiveModel;
	}

	@Override
	public String toString() {
		return "AdEntity [name=" + name + ", description=" + description + ", camapignId=" + campaignId + ", objectId="
				+ objectId + ", budget=" + budget + ", boost=" + boost + ", persona=" + persona + ", startTime="
				+ startTime + ", endTime=" + endTime + ", costPerClick=" + costPerClick + ", costPerView=" + costPerView
				+ ", frequencyCap=" + frequencyCap + ", predictiveModel=" + predictiveModel + ", id=" + id
				+ ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", lastModifiedOn=" + lastModifiedOn
				+ ", version=" + version + "]";
	}



	

	
}
