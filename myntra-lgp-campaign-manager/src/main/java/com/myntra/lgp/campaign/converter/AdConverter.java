package com.myntra.lgp.campaign.converter;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.lgp.campaign.client.entry.AdEntry;
import com.myntra.lgp.campaign.dao.CampaignDao;
import com.myntra.lgp.campaign.entity.AdEntity;

public class AdConverter {
	
	@Autowired
	private ObjectMapper jsonMapper;

	@Autowired
	private CampaignDao campaignDao;
	
	public AdEntity convertToAdEntity(AdEntry a){
		AdEntity adEntity = new AdEntity();
		adEntity.setBoost(a.getBoost());
		adEntity.setBudget(a.getBudget());
		String campaignId = a.getCampaignId();
		adEntity.setCampaignId(campaignDao.findByCampaignId(campaignId));
		adEntity.setCostPerClick(a.getCostPerClick());
		adEntity.setCostPerView(a.getCostPerView());
		adEntity.setDescription(a.getDescription());
		adEntity.setEndTime(a.getEndTime());
		adEntity.setFrequencyCap(a.getFrequencyCap());
		adEntity.setName(a.getName());
		adEntity.setObjectId(a.getObjectId());
		
		try {
			adEntity.setPersona(jsonMapper.writeValueAsString(a.getPersona()));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		adEntity.setStartTime(a.getStartTime());
		
		return adEntity;
	}

}
