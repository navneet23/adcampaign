package com.myntra.lgp.campaign.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;


public class JsonUtils {

	private static ObjectMapper mapper = new ObjectMapper();
	private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtils.class);

	static {
		mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
		mapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
		mapper.enable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
	}

	public static <T> String marshal(T obj) {
		try {
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			LOGGER.error("Cannot be Serialized", e);
		}
		return null;
	}

	public static <T> T unmarshal(InputStream input, Class<T> c) {
		try {
			return mapper.readValue(input, c);
		} catch (Exception e) {
			LOGGER.error("Cannot be DeSerialized", e);
		}
		return null;
	}

	public static <T> T unmarshal(String input, Class<T> c) {

		ByteArrayInputStream stream = null;
		try {
			stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
			return mapper.readValue(stream, c);
		} catch (IOException e) {
			LOGGER.error("Cannot be DeSerialized", e);
		} finally {
			if (stream != null)
				try {
					stream.close();
				} catch (IOException e) {
				}
		}
		return null;
	}

	public static <T> T unmarshal(byte[] input, Class<T> c) {

		ByteArrayInputStream stream = null;
		try {
			stream = new ByteArrayInputStream(input);
			return mapper.readValue(stream, c);
		} catch (IOException e) {
			LOGGER.error("Cannot be DeSerialized", e);
		} finally {
			if (stream != null)
				try {
					stream.close();
				} catch (IOException e) {
				}
		}
		return null;
	}

	public static <T> T unmarshal(String input, TypeReference<T> t) {

		ByteArrayInputStream stream = null;
		try {
			stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
			return mapper.readValue(stream, t);
		} catch (IOException e) {
			LOGGER.error("Cannot be DeSerialized", e);
		} finally {
			if (stream != null)
				try {
					stream.close();
				} catch (IOException e) {
				}
		}
		return null;
	}

	public static <T> T unmarshal(byte[] input, TypeReference<T> t) {

		ByteArrayInputStream stream = null;
		try {
			stream = new ByteArrayInputStream(input);
			return mapper.readValue(stream, t);
		} catch (IOException e) {
			LOGGER.error("Cannot be DeSerialized", e);
		} finally {
			if (stream != null)
				try {
					stream.close();
				} catch (IOException e) {
				}
		}
		return null;
	}

	public static <T> T unmarshal(InputStream input, TypeReference<T> t) {
		try {
			return mapper.readValue(input, t);
		} catch (IOException e) {
			LOGGER.error("Cannot be DeSerialized", e);
		}
		return null;
	}

	public static <T> T convertValues(Object obj, Class<T> _class) {
		return mapper.convertValue(obj, _class);
	}

	public static JsonNode createJsonNode() {
		return mapper.createObjectNode();
	}

}
