package com.myntra.lgp.campaign.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.myntra.commons.entities.BaseEntity;

@Entity
@Table(name="account")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property = "id", scope=AccountEntity.class)
public class AccountEntity extends BaseEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1554508728265808871L;

	@Column(name="name")
	private String name;
	
	@Column(name="type")
	private String type;
	
	@Column(name="balance")
	private Long balance;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="advertiser_id", updatable=false)
	@NotNull
	private AdvertiserEntity advertiserRefId;
	
	@OneToMany(fetch = FetchType.EAGER , mappedBy="accountName")
	private Set<CampaignEntity> campaigns;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getBalance() {
		return balance;
	}

	public void setBalance(Long balance) {
		this.balance = balance;
	}

	public AdvertiserEntity getAdvertiserRefId() {
		return advertiserRefId;
	}

	public void setAdvertiserRefId(AdvertiserEntity advertiserInfo) {
		this.advertiserRefId = advertiserInfo;
	}

	public Set<CampaignEntity> getCampaigns() {
		return campaigns;
	}

	public void setCampaigns(Set<CampaignEntity> campaigns) {
		this.campaigns = campaigns;
	}

	@Override
	public String toString() {
		return "AccountEntity [name=" + name + ", type=" + type + ", balance=" + balance + ", id=" + id + ", createdBy="
				+ createdBy + ", createdOn=" + createdOn + ", lastModifiedOn=" + lastModifiedOn + ", version=" + version
				+ "]";
	}	
	

}
