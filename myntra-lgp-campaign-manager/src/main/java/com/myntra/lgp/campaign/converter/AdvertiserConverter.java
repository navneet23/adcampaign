package com.myntra.lgp.campaign.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.myntra.lgp.campaign.client.entry.AdvertiserEntry;
import com.myntra.lgp.campaign.entity.AdvertiserEntity;

public class AdvertiserConverter {
	
	@Autowired
	private DozerBeanMapper beanMapper;
	
	public  AdvertiserEntity convertToAdvertiser(AdvertiserEntry a){
		
//		AdvertiserEntity converted = new AdvertiserEntity();
		AdvertiserEntity converted = beanMapper.map(a, AdvertiserEntity.class);
//		
//		converted.setAdvertiserRefId(a.getAdvertiserId());
//		converted.setName(a.getName());
//		converted.setDescription(a.getDescription());
		return converted;
		
	}
	
	public AdvertiserEntry convertToAdvertiserEntry(AdvertiserEntity a){
		AdvertiserEntry converted = beanMapper.map(a, AdvertiserEntry.class);
		return converted;
	}

}
