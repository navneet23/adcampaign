package com.myntra.lgp.campaign.dao;

import org.springframework.data.repository.CrudRepository;

import com.myntra.lgp.campaign.entity.AdvertiserEntity;


public interface AdvertisorDao extends CrudRepository<AdvertiserEntity, Long>{
	
	public AdvertiserEntity findById(Long id);
	
	public AdvertiserEntity findByAdvertiserRefId(String id);
	
	

}
