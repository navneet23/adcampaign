package com.myntra.lgp.campaign.converter;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.myntra.lgp.campaign.client.entry.CampaignEntry;
import com.myntra.lgp.campaign.dao.AccountDao;
import com.myntra.lgp.campaign.entity.AccountEntity;
import com.myntra.lgp.campaign.entity.CampaignEntity;

public class CampaignConverter {

	@Autowired
	private DozerBeanMapper beanMapper;

	@Autowired
	private AccountDao accountDao;

	public CampaignEntity convertToCampaignEntity(CampaignEntry c) {
		//
		// AccountEntity converted = new AccountEntity();
		// converted.setBalance(a.getBalance());
		// converted.setCreatedBy(a.getCreatedBy());
		// converted.setCreatedOn(a.getCreatedOn());
		// converted.setLastModifiedOn(a.getLastModifiedOn());
		// converted.setName(a.getName());
		// converted.setType(a.getType());
		// converted.setVersion(a.getVersion());
		// String refId = a.getAdvertiserRefId();
		// converted.setAdvertiserRefId(advertiserDao.findByAdvertiserRefId(refId));

		CampaignEntity converted = new CampaignEntity();

		converted.setBudget(c.getBudget());
		converted.setCampaignId(c.getCampaignId());
		converted.setDescription(c.getDescription());
		converted.setEndTime(c.getEndTime());
		converted.setStartTime(c.getStartTime());
		converted.setName(c.getName());
		String accountName = c.getAccountName();
		converted.setAccountName(accountDao.findByName(accountName));
		return converted;

	}

	public CampaignEntry convertToCampaignEntry(CampaignEntity c) {

		CampaignEntry converted = beanMapper.map(c, CampaignEntry.class);
		return converted;
	}

	// public CampaignEntry convertTo
}
