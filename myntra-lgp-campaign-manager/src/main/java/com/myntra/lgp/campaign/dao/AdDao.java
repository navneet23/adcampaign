package com.myntra.lgp.campaign.dao;

import org.springframework.data.repository.CrudRepository;

import com.myntra.lgp.campaign.entity.AdEntity;

public interface AdDao extends CrudRepository<AdEntity, Long> {

}
