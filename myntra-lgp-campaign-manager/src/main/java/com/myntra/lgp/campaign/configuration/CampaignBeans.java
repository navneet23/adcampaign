package com.myntra.lgp.campaign.configuration;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.lgp.campaign.converter.AccountConverter;
import com.myntra.lgp.campaign.converter.AdConverter;
import com.myntra.lgp.campaign.converter.AdvertiserConverter;
import com.myntra.lgp.campaign.converter.CampaignConverter;
import com.myntra.lgp.campaign.managers.AdvertiserManager;

@Configuration
public class CampaignBeans {
	@Bean
	DozerBeanMapper beanMapper(){
		DozerBeanMapper beanMapper = new DozerBeanMapper();
		return beanMapper;
	}
	
	@Bean
	ObjectMapper jsonMapper(){
		ObjectMapper jsonMapper = new ObjectMapper();
		return jsonMapper;
	}
	
	@Bean 
	AdvertiserConverter advertiserConverter(){
		AdvertiserConverter advertiserConverter = new AdvertiserConverter();
		return advertiserConverter;
	}
	
	
	@Bean
	AccountConverter accountConverter(){
		AccountConverter accountConverter = new AccountConverter();
		return accountConverter;
		
	}
	
	@Bean 
	CampaignConverter campaignConverter(){
		CampaignConverter campaignConverter = new CampaignConverter();
		return campaignConverter;
	}
	
	
	@Bean
	AdConverter adConverter(){
		AdConverter adConverter = new AdConverter();
		return adConverter;
	}
	
	@Bean
	AdvertiserManager advertiserManager(){
		AdvertiserManager advertiserManager = new AdvertiserManager();
		return advertiserManager;
	}
}
