package com.myntra.lgp.campaign.entity;


import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.myntra.commons.entities.BaseEntity;

@Entity
@Table(name="advertiser")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property = "id", scope=AdvertiserEntity.class)
public class AdvertiserEntity extends BaseEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1662257972297534009L;

	@Column(name ="name")
	private String name;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "advertiser_ref_id")
	private String advertiserRefId;
	

	@OneToMany(fetch = FetchType.EAGER , cascade={CascadeType.ALL}, mappedBy="advertiserRefId")
	private Set<AccountEntity> accounts;

	public Set<AccountEntity> getAccounts() {
		return accounts;
	}

	public void setAccounts(Set<AccountEntity> accounts) {
		this.accounts = accounts;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getAdvertiserRefId() {
		return advertiserRefId;
	}

	
	public void setAdvertiserRefId(String advertiser_id) {
		this.advertiserRefId = advertiser_id;
	}
	
	

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((advertiserRefId == null) ? 0 : advertiserRefId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdvertiserEntity other = (AdvertiserEntity) obj;
		if (advertiserRefId == null) {
			if (other.advertiserRefId != null)
				return false;
		} else if (!advertiserRefId.equals(other.advertiserRefId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AdvertiserEntity [name=" + name + ", description=" + description + ", advertiserRefId="
				+ advertiserRefId + ", id=" + id + ", createdBy=" + createdBy + ", createdOn=" + createdOn
				+ ", lastModifiedOn=" + lastModifiedOn + ", version=" + version + "]";
	}
	
	
	

}
