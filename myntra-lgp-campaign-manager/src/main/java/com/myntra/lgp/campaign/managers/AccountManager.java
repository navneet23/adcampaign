package com.myntra.lgp.campaign.managers;

import org.springframework.beans.factory.annotation.Autowired;

import com.myntra.commons.entries.BaseEntry;
import com.myntra.lgp.campaign.client.entry.AccountEntry;
import com.myntra.lgp.campaign.converter.AccountConverter;
import com.myntra.lgp.campaign.dao.AccountDao;
import com.myntra.lgp.campaign.entity.AccountEntity;

public class AccountManager implements Manager {

	@Autowired
	private AccountDao accountDao;
	
	@Autowired
	private AccountConverter accountConverter;

	@Override
	public Object handleUpdate(BaseEntry b) {
		// TODO Auto-generated method stub

		AccountEntry a = (AccountEntry) b;

		return null;
	}

	@Override
	public Object handleCreate(BaseEntry b) {
		// TODO Auto-generated method stub

		AccountEntry a = (AccountEntry) b;

		AccountEntity tableEntry = accountConverter.convertToAaccountEntity(a);

		accountDao.save(tableEntry);
		return "Fine";
	}

	@Override
	public Object handleGet(String key) {
		// TODO Auto-generated method stub
		AccountEntity account = accountDao.findByName(key);
		return account;

	}

	@Override
	public Object handleDelete(String key) {
		// TODO Auto-generated method stub
		AccountEntity account = accountDao.findByName(key);
		accountDao.delete(account);

		return "DELETED";
	}

}
