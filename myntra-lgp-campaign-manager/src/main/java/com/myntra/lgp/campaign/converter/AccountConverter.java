package com.myntra.lgp.campaign.converter;

import java.util.HashSet;
import java.util.Set;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.myntra.lgp.campaign.client.entry.AccountEntry;
import com.myntra.lgp.campaign.dao.AdvertisorDao;
import com.myntra.lgp.campaign.entity.AccountEntity;
import com.myntra.lgp.campaign.entity.AdvertiserEntity;

public class AccountConverter {
	
	@Autowired
	private DozerBeanMapper beanMapper;
	
	@Autowired
	private AdvertisorDao advertiserDao;
	
	
	public  AccountEntity convertToAaccountEntity(AccountEntry a){
		
		AccountEntity converted = new AccountEntity();
		converted.setBalance(a.getBalance());
		converted.setCreatedBy(a.getCreatedBy());
		converted.setCreatedOn(a.getCreatedOn());
		converted.setLastModifiedOn(a.getLastModifiedOn());
		converted.setName(a.getName());
		converted.setType(a.getType());
		converted.setVersion(a.getVersion());
		String refId = a.getAdvertiserRefId();
		converted.setAdvertiserRefId(advertiserDao.findByAdvertiserRefId(refId));
		

		 
				
		return converted;
	}
	
	
	public AccountEntry convertToEntry(AccountEntity a){
	return null;
	}
}
