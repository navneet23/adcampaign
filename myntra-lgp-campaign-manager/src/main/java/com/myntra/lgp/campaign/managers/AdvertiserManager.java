package com.myntra.lgp.campaign.managers;

import org.springframework.beans.factory.annotation.Autowired;

import com.myntra.commons.entries.BaseEntry;
import com.myntra.lgp.campaign.client.entry.AdvertiserEntry;
import com.myntra.lgp.campaign.converter.AdvertiserConverter;
import com.myntra.lgp.campaign.dao.AdvertisorDao;
import com.myntra.lgp.campaign.entity.AdvertiserEntity;

public class AdvertiserManager implements Manager{
	
	@Autowired
	private AdvertisorDao advertiserDao;
	
	@Autowired
	private AdvertiserConverter advertiserConverter;
	
	@Override
	public Object handleUpdate(BaseEntry b){
		AdvertiserEntry a = (AdvertiserEntry) b;
		String advertiserRefId = a.getAdvertiserRefId();
		AdvertiserEntity advertiserObject = advertiserDao.findByAdvertiserRefId(advertiserRefId);
		if(advertiserObject == null){
			return "No Such Advertiser";
		}
		
		else{
			advertiserObject.setCreatedBy(a.getCreatedBy());
			advertiserObject.setCreatedOn(a.getCreatedOn());
			advertiserObject.setDescription(a.getDescription());
			advertiserObject.setCreatedBy(a.getCreatedBy());
			advertiserObject.setLastModifiedOn(a.getLastModifiedOn());
			advertiserObject.setVersion(a.getVersion());
			advertiserObject.setName(a.getName());
			advertiserDao.save(advertiserObject);
			
			return "Updated";
		}
	}
	
	@Override
	public Object handleGet(String refId){
		AdvertiserEntity ret = advertiserDao.findByAdvertiserRefId(refId);
		System.out.println(ret);
		return ret;
	}
	
	@Override
	public Object handleCreate(BaseEntry b){
		
		AdvertiserEntry a = (AdvertiserEntry) b;
		
		AdvertiserEntity c = advertiserConverter.convertToAdvertiser(a);
		advertiserDao.save(c);
		return "OK";
		
	}
	
	public Object handleDelete(String refId){
		AdvertiserEntity ret = advertiserDao.findByAdvertiserRefId(refId);
		advertiserDao.delete(ret);
		return "DELETED";
	}
	
	
}
