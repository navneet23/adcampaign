package com.myntra.lgp.campaign.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.myntra.commons.entities.BaseEntity;

@Entity
@Table(name = "campaign")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property = "id", scope=CampaignEntity.class)
public class CampaignEntity extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "start_time")
	private Long startTime;

	@Column(name = "end_time")
	private Long endTime;

	@Column(name = "budget")
	private Long budget;

	@Column(name = "campaign_id")
	private String campaignId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "account_id", updatable = false)
	@NotNull
	private AccountEntity accountName;

	@OneToMany(fetch = FetchType.LAZY , mappedBy="campaignId")
	private Set<AdEntity> ads;

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	} 

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Long getBudget() {
		return budget;
	}

	public void setBudget(Long budget) {
		this.budget = budget;
	}

	public Set<AdEntity> getAds() {
		return ads;
	}

	public void setAds(Set<AdEntity> ads) {
		this.ads = ads;
	}

	public AccountEntity getAccountName() {
		return accountName;
	}

	public void setAccountName(AccountEntity accountId) {
		this.accountName = accountId;
	}

	@Override
	public String toString() {
		return "CampaignEntity [name=" + name + ", description=" + description + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", budget=" + budget + ", campaignId=" + campaignId + ", ads=" + ads + "]";
	}

	

}
