package com.myntra.lgp.campaign.dao;

import org.springframework.data.repository.CrudRepository;

import com.myntra.lgp.campaign.entity.CampaignEntity;

public interface CampaignDao extends CrudRepository<CampaignEntity, Long> {
	
	public CampaignEntity findById(Long id);
	
	public CampaignEntity findByCampaignId(String campaignId);

}
