package main.java;

import org.dozer.DozerBeanMapper;

public class DozerMapperTest {

	public static void main(String args[]){
		
		DozerBeanMapper mapper = new DozerBeanMapper();
		A a = new A();
		a.setA1("one");
		a.setA2("two");
		
		B b = mapper.map(a, B.class);
		
		System.out.println(b);
		
	}
}


