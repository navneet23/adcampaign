CREATE TABLE `advertiser` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `last_modified_on` datetime NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  `advertiser_ref_id` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `last_modified_on` datetime NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  `balance` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `advertiser_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKc8xjs1nehvf4u66or3t0ald75` (`advertiser_id`),
  CONSTRAINT `FKc8xjs1nehvf4u66or3t0ald75` FOREIGN KEY (`advertiser_id`) REFERENCES `advertiser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `ad` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `last_modified_on` datetime NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  `budget` bigint(20) DEFAULT NULL,
  `cost_per_click` float DEFAULT NULL,
  `cost_per_view` float DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `end_time` bigint(20) DEFAULT NULL,
  `frequency_cap` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `object_id` varchar(255) DEFAULT NULL,
  `persona` tinyblob DEFAULT NULL,
  `start_time` bigint(20) DEFAULT NULL,
  `boost` float DEFAULT NULL,
  `predictive_model` tinyblob,
  `campaign_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8ix9teswwwus5x64afej4v604` (`campaign_id`),
  CONSTRAINT `FK8ix9teswwwus5x64afej4v604` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;


CREATE TABLE `campaign` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `last_modified_on` datetime NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  `budget` bigint(20) DEFAULT NULL,
  `campaign_id` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `end_time` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `start_time` bigint(20) DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKlqesxxnp9uqmams3lg7vvlk01` (`account_id`),
  CONSTRAINT `FKlqesxxnp9uqmams3lg7vvlk01` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;