package com.myntra.lgp.campaign.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.myntra.lgp.campaign.client.entry.AdvertiserEntry;
import com.myntra.lgp.campaign.converter.AdvertiserConverter;
import com.myntra.lgp.campaign.dao.AdvertisorDao;
import com.myntra.lgp.campaign.entity.AdvertiserEntity;
import com.myntra.lgp.campaign.managers.AdvertiserManager;
import com.myntra.lgp.campaign.service.AdvertiserService;

@RestController
public class AdvertiserServiceImpl implements AdvertiserService {

	
	@Autowired
	private AdvertiserManager advertiserManager;

	public Object delete(@PathVariable("id")String id) {
		return advertiserManager.handleDelete(id);
	}

	

	@Override
	public Object get(@PathVariable("id") String id) {
		return advertiserManager.handleGet(id);
	}

	@Override
	public Object create(@RequestBody AdvertiserEntry a) {
		return advertiserManager.handleCreate(a);
	}



	@Override
	public Object update(@RequestBody AdvertiserEntry a) {
		// TODO Auto-generated method stub
		
		return advertiserManager.handleUpdate(a);
		
	}
	
	
}
