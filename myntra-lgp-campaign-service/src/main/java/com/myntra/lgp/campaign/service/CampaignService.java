package com.myntra.lgp.campaign.service;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.myntra.lgp.campaign.client.entry.CampaignEntry;

@RestController
@RequestMapping("/campaign")
public interface CampaignService {
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public Object create(@RequestBody CampaignEntry a);

	@RequestMapping(value = "/delete/{campaignId}", method = RequestMethod.DELETE)
	public Object delete(@PathVariable("camapignId") String id);

	@RequestMapping(value = "/update/{campaignId}/", method = RequestMethod.PUT)
	public Object update(@PathVariable("camapignId") String id);

	@RequestMapping(value = "/{campaignId}", method = RequestMethod.GET)
	public Object get(@PathVariable("campaignId") String id);
	
	@RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
	public Object getWithId(@PathVariable("id") String id);
	
	
}
