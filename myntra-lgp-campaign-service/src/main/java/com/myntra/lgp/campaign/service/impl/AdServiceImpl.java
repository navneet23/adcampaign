package com.myntra.lgp.campaign.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.myntra.lgp.campaign.client.entry.AdEntry;
import com.myntra.lgp.campaign.config.AmqpMessageScheduler;
import com.myntra.lgp.campaign.converter.AdConverter;
import com.myntra.lgp.campaign.dao.AdDao;
import com.myntra.lgp.campaign.entity.AdEntity;
import com.myntra.lgp.campaign.service.AdService;

@RestController
public class AdServiceImpl implements AdService {

	@Autowired
	private AmqpMessageScheduler amqpMessageScheduler;
	
	@Autowired
	private AdDao adDao;
	
	@Autowired
	private AdConverter adConverter;
	
	@Override
	public Object create(@RequestBody AdEntry a) {
		
		AdEntity adEntity  = adConverter.convertToAdEntity(a);
		
		adDao.save(adEntity);
		
		//System.out.println("HELLO WORLD");
		
		//messageSender.sendMessageToQueue(a, "schedulerInputQ");
		//return null;
		System.out.println("HELLO WORLD");
		amqpMessageScheduler.sendMessageToQueue(a, "pumps" , "temp", System.currentTimeMillis()+60000);
		return "OK";
	}

	@Override
	public Object delete(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object update(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object get(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
