package com.myntra.lgp.campaign.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;


@Configuration
public class RabbitConfig {
	

   private final static String DEFAULT_QUEUE_NAME = "schedulerInputQ";
   private final static String DEFAULT_EXCHANGE = "";

   @Autowired
   public Environment env ;


   public String queueName(){
	   return env.getProperty("spe.queueName", DEFAULT_QUEUE_NAME);
   }

   public String exchangeName(){
	   return env.getProperty("spe.exchangeName", DEFAULT_EXCHANGE);
   }

   
    @Bean
    Queue queue() {
        return new Queue(queueName());
    }

    @Bean
    Queue schedulerInputQ() {
        return new Queue("schedulerInputQ");
    }
    
    @Bean
    TopicExchange exchange() {
        return new TopicExchange(exchangeName());
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(queueName());
    }

//    @Bean
//    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
//            MessageListenerAdapter listenerAdapter) {
//        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
//        container.setConnectionFactory(connectionFactory);
//        container.setQueueNames(queueName());
//        container.setMessageListener(listenerAdapter);
//        return container;
//    }

//    @Bean
//    SPEReceiver receiver() {
//        return new SPEReceiver();
//    }
//
//    @Bean
//    MessageListenerAdapter listenerAdapter(SPEReceiver receiver) {
//        return new MessageListenerAdapter(receiver, "receiveMessage");
//    }

}
