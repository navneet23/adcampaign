package com.myntra.lgp.campaign.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.myntra.lgp.campaign.client.entry.AdvertiserEntry;
import com.myntra.lgp.campaign.client.entry.CampaignEntry;
import com.myntra.lgp.campaign.converter.CampaignConverter;
import com.myntra.lgp.campaign.dao.CampaignDao;
import com.myntra.lgp.campaign.entity.CampaignEntity;
import com.myntra.lgp.campaign.service.CampaignService;


@RestController
public class CampaignServiceImpl implements CampaignService {
	
	@Autowired
	private CampaignDao campaignDao;
	
	@Autowired
	private CampaignConverter campaignConverter;
	
	
	
	

	@Override
	public Object create(@RequestBody CampaignEntry a) {
		// TODO Auto-generated method stub
		CampaignEntity entity = campaignConverter.convertToCampaignEntity(a);
		campaignDao.save(entity);		
		return "OK";
	}

	@Override
	public Object delete(@PathVariable("campaignId")String campaignId) {
		// TODO Auto-generated method stub
		
		CampaignEntity found = campaignDao.findByCampaignId(campaignId);
		campaignDao.delete(found);
		return "DELETE";
		}

	
	@Override
	public Object update(String CampaignId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object get(@PathVariable("campaignId")String campaignId) {
		// TODO Auto-generated method stub
		
		CampaignEntity found = campaignDao.findByCampaignId(campaignId);
		System.out.println(found);
		return found;
		}

	@Override
	public Object getWithId(@PathVariable("id")String id) {
		// TODO Auto-generated method stub
		CampaignEntity found = campaignDao.findById(Long.parseLong(id));
		return found;
	}

	  

}
