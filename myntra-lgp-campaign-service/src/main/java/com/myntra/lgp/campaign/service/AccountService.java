package com.myntra.lgp.campaign.service;

import javax.transaction.Transactional;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.myntra.lgp.campaign.client.entry.AccountEntry;

@RestController
@Transactional
@RequestMapping("/account")
public interface AccountService {

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public Object create(@RequestBody AccountEntry a);
	
	@RequestMapping(value = "/{name}", method = RequestMethod.GET)
	public Object get(@PathVariable("name") String id);
	
	@RequestMapping(value = "delete/{name}", method = RequestMethod.DELETE)
	public Object delete(@PathVariable("name") String id);

	@RequestMapping(value = "/update/{name}/", method = RequestMethod.PUT)
	public Object update(@PathVariable("name") String id);

}
