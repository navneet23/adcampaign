package com.myntra.lgp.campaign.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.myntra.lgp.campaign.client.entry.AccountEntry;
import com.myntra.lgp.campaign.converter.AccountConverter;
import com.myntra.lgp.campaign.dao.AccountDao;
import com.myntra.lgp.campaign.entity.AccountEntity;
import com.myntra.lgp.campaign.service.AccountService;

@RestController
public class AccountServiceImpl implements AccountService{
	
	@Autowired
	private AccountDao accountDao;
	
	@Autowired
	private AccountConverter accountConverter;

	@Override
	public Object create(@RequestBody AccountEntry a) {
		AccountEntity tableEntry = accountConverter.convertToAaccountEntity(a);
		
		accountDao.save(tableEntry);
		return "Fine";
	}

	@Override
	public Object get(@PathVariable("name") String name) {
		// TODO Auto-generated method stub
		AccountEntity account = accountDao.findByName(name);
		return account;
	}

	@Override
	public Object delete(@PathVariable("name")String name) {
		// TODO Auto-generated method stub
		AccountEntity account = accountDao.findByName(name);
		accountDao.delete(account);
		return null;
	}

	@Override
	public Object update(@PathVariable("name")String name) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
