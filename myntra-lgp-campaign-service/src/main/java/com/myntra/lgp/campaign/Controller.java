package com.myntra.lgp.campaign;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface Controller {
	
	@RequestMapping(value="/create",method=RequestMethod.GET)
	public Object create();
	
	@RequestMapping(value="/delete",method=RequestMethod.GET)
	public Object delete();
	
	
	@RequestMapping(value="/update",method=RequestMethod.GET)
	public Object update();
	
	@RequestMapping(value="/get",method=RequestMethod.GET)
	public Object get();
	
	
}
