package com.myntra.lgp.campaign.config;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.lgp.campaign.utils.JsonUtils;

@Component
public class AmqpMessageScheduler {

	public static final String schedulerQueueRoute = "schedulerInputQ";
	public static final String schedulerQueueExchange = "";

	private RabbitTemplate rabbitTemplate;
	private ConfigurableApplicationContext context;

	@Autowired
	private ObjectMapper objectMapper;

	public AmqpMessageScheduler(RabbitTemplate rabbitTemplate,
			ConfigurableApplicationContext context) {
		this.rabbitTemplate = rabbitTemplate;
		this.context = context;
	}

	public void sendMessageToQueue(Object payload, String targetExchange,
			String targetRoutingKey, Long scheduledTime) {

		final MessageProperties messageProperties = new MessageProperties();
		messageProperties.setContentType(MessageProperties.CONTENT_TYPE_JSON);

		Map<String, Object> schedulerPayload = new HashMap<String, Object>();

		schedulerPayload.put("cbData", payload);
		schedulerPayload.put("schedule", scheduledTime);
		schedulerPayload.put("cbExchange", targetExchange);
		schedulerPayload.put("cbRoute", targetRoutingKey);

		final Message message = new Message(JsonUtils.marshal(schedulerPayload)
				.getBytes(), messageProperties);

		rabbitTemplate.convertAndSend(schedulerQueueExchange,
				schedulerQueueRoute, message);
	}

}
