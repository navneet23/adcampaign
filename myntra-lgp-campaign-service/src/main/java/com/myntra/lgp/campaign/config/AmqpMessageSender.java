package com.myntra.lgp.campaign.config;

import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.lgp.campaign.utils.JsonUtils;

import org.springframework.amqp.core.Message;

// This class is  Only for debugging purpose
@Component
public class AmqpMessageSender {

	private RabbitTemplate rabbitTemplate;
	private ConfigurableApplicationContext context;

	@Autowired
	private ObjectMapper objectMapper;

	public AmqpMessageSender(RabbitTemplate rabbitTemplate,
			ConfigurableApplicationContext context) {
		this.rabbitTemplate = rabbitTemplate;
		this.context = context;
	}

	public void sendMessageToQueue(Object payload, String exchange, String routingKey) {

		final MessageProperties messageProperties = new MessageProperties();
		messageProperties.setContentType(MessageProperties.CONTENT_TYPE_JSON);
		
		final Message message = new Message(JsonUtils.marshal(payload)
				.getBytes(), messageProperties);
		
		rabbitTemplate.convertAndSend(exchange , routingKey, message);
	}

}
