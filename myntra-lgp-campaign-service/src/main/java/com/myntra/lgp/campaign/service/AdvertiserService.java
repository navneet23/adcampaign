package com.myntra.lgp.campaign.service;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.myntra.lgp.campaign.client.entry.AdvertiserEntry;

@RestController
@RequestMapping("/advertiser")
public interface AdvertiserService {

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public Object create(@RequestBody AdvertiserEntry a);

	@RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
	public Object delete(@PathVariable("id") String id);

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public Object update(@RequestBody AdvertiserEntry a);

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Object get(@PathVariable("id") String id);

}
