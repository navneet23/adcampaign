package com.myntra.lgp.campaign.client.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator.Feature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;
import com.myntra.commons.auth.ContextInfo;
import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.utils.ServiceURLProperty;
import com.myntra.lgp.campaign.client.entry.AccountEntry;
import com.myntra.lgp.campaign.client.entry.AdEntry;
import com.myntra.lgp.campaign.client.entry.AdvertiserEntry;
import com.myntra.lgp.campaign.client.entry.CampaignEntry;
import com.myntra.lgp.campaign.client.exception.AdCampaignClientException;
import com.myntra.lgp.campaign.client.response.ApiResponse;
import com.myntra.lgp.campaign.client.response.BaseResponse;


public class AdCampaignClient {
	
	private static final String SERVICE_PREFIX = "/idea/opt";
	private static Logger logger = LoggerFactory.getLogger(AdCampaignClient.class);
	private String url;
	private HttpClient httpClient;

	private static ObjectMapper mapper = new ObjectMapper();

	static {
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
		mapper.configure(Feature.ESCAPE_NON_ASCII, true);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public AdCampaignClient(String url) {
		this.url = url;
		PoolingClientConnectionManager pccm = new PoolingClientConnectionManager();
		BasicHttpParams params = new BasicHttpParams();
		pccm.setDefaultMaxPerRoute(1000);
		pccm.setMaxTotal(1000);
		httpClient = new DefaultHttpClient(pccm, params);

	}

	private static <T> T notNull(T response, String errorMessage) {
		if (response == null)
			throw new AdCampaignClientException(errorMessage);
		return response;
	}

	private HttpEntity toHttpEntity(String str) {
		try {
			return new StringEntity(str);
		} catch (UnsupportedEncodingException e) {
			throw new AdCampaignClientException(e);
		}
	}

//	
//	public AuthenticationResponse authenticate(AuthenticationPhoneEntry entry) {
//		HttpPost http = new HttpPost(url + SERVICE_PREFIX + "/auth/phone");
//		http.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.IGNORE_COOKIES);
//		http.setEntity(toHttpEntity(marshal(entry)));
//		String errorMessage = "Unable to do authentication";
//		return notNull(executeHttpRequest(http, AuthenticationResponse.class), errorMessage);

	
	public ApiResponse getAccounts(AdvertiserEntry a) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ApiResponse createAdvertisor(AdvertiserEntry a) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ApiResponse deleteAdvertisor(AdvertiserEntry a) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ApiResponse updateAdvertisor(AdvertiserEntry a) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ApiResponse getCampaigns(AccountEntry a) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ApiResponse createAccount(AccountEntry a) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ApiResponse deleteAccount(AccountEntry a) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ApiResponse updateAccount(AccountEntry a) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ApiResponse getAds(CampaignEntry c) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ApiResponse createCampaign(CampaignEntry c) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ApiResponse deleteCampaign(CampaignEntry c) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ApiResponse updateCampaign(CampaignEntry c) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ApiResponse createAd(AdEntry a) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ApiResponse deleteAd(AdEntry a) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ApiResponse updateAd(AdEntry a) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public BaseResponse<AdEntry> getActiveAds() { 
		HttpGet http = new HttpGet(url + SERVICE_PREFIX );
		http.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.IGNORE_COOKIES);
		String errorMessage = "Unable to do get Profile";
		return notNull(executeHttpRequest(http, BaseResponse.class), errorMessage);
		
	}
	
	private <T> T executeHttpRequest(HttpRequestBase httprequest, Class<T> _class) {

		httprequest.addHeader("Accept", "application/json");
		httprequest.addHeader("Content-Type", "application/json");
		HttpResponse httpResponse = null;
		HttpEntity entity = null;
		try {
			httpResponse = httpClient.execute(httprequest);
			entity = httpResponse.getEntity();
			if (entity != null) {
				return unmarshal(entity.getContent(), _class);
			}
		} catch (Exception e) {
			throw new AdCampaignClientException(e);
		} finally {
			try {
				EntityUtils.consume(entity);
			} catch (Exception e) {
				logger.error("Failed to consume entity", e);
			}
		}
		return null;
	}

	public static <T> String marshal(T obj) {

		try {
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static <T> T unmarshal(InputStream input, Class<T> c) {
		try {
			return mapper.readValue(input, c);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static <T> T unmarshal(byte[] input, Class<T> c) {
		try {
			return mapper.readValue(input, c);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static <T> T unmarshal(InputStream input, TypeReference<T> t) {
		try {
			return mapper.readValue(input, t);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return null;
	}

	public static <T> T unmarshal(byte[] input, TypeReference<T> t) {
		try {
			return mapper.readValue(input, t);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static <T> T convertValues(Object obj, Class<T> _class) {
		return mapper.convertValue(obj, _class);
	}

	
}
