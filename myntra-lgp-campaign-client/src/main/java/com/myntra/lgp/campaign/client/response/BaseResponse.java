package com.myntra.lgp.campaign.client.response;

import java.util.List;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.response.AbstractResponse;

public class BaseResponse<T> extends AbstractResponse{

	private static final long serialVersionUID = 1792514890989567582L;
	
	private List<T> data;

	public BaseResponse() {}

	public BaseResponse(StatusResponse status) {
		super(status);
	}

	public BaseResponse(List<T> objectEntries) {
		this.data = objectEntries;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
		if(getStatus()!=null && data != null){
			getStatus().setTotalCount(data.size());
		}
	}

	@Override
	public String toString() {
		return "BaseResponse [data=" + data + ", getStatus()="
				+ getStatus() + "]";
	}


}
