package com.myntra.lgp.campaign.client.entry;

import java.util.Map;
import java.util.TreeMap;

import com.myntra.commons.entries.BaseEntry;

public class AdEntry extends BaseEntry{
	
	private String name;
	
	private String description;
	
	private String campaignId;
	
	private String objectId;
	
	private Long budget;
	
	//private String persona;
	
	// location, map...
	
	// predictive object from BI..
	//boost
	private Float boost;
	
	private Map<String,Object> persona;
	
	private Long startTime;
	
	private Long endTime;
	
	private Float costPerClick;
	
	private Float costPerView;
	
	private Integer frequencyCap;
	
	//private TreeMap<Long,Long> predictiveModel;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String camapignInfo) {
		this.campaignId = camapignInfo;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public Long getBudget() {
		return budget;
	}

	public void setBudget(Long budget) {
		this.budget = budget;
	}

	public Map<String, Object> getPersona() {
		return persona;
	}

	public void setPersona(Map<String, Object> persona) {
		this.persona = persona;
	}

	public Float getBoost() {
		return boost;
	}

	public void setBoost(Float boost) {
		this.boost = boost;
	}

//	public TreeMap<Long, Long> getPredictiveModel() {
//		return predictiveModel;
//	}
//
//	public void setPredictiveModel(TreeMap<Long, Long> predictiveModel) {
//		this.predictiveModel = predictiveModel;
//	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Float getCostPerClick() {
		return costPerClick;
	}

	public void setCostPerClick(Float costPerClick) {
		this.costPerClick = costPerClick;
	}

	public Float getCostPerView() {
		return costPerView;
	}

	public void setCostPerView(Float costPerView) {
		this.costPerView = costPerView;
	}

	public Integer getFrequencyCap() {
		return frequencyCap;
	}

	public void setFrequencyCap(Integer frequencyCap) {
		this.frequencyCap = frequencyCap;
	}

	@Override
	public String toString() {
		return "AdEntry [name=" + name + ", description=" + description + ", campaignId=" + campaignId + ", objectId="
				+ objectId + ", budget=" + budget + ", boost=" + boost + ", persona=" + persona + ", startTime="
				+ startTime + ", endTime=" + endTime + ", costPerClick=" + costPerClick + ", costPerView=" + costPerView
				+ ", frequencyCap=" + frequencyCap + "]";
	}

	
	
	

	
	
	
	
}
