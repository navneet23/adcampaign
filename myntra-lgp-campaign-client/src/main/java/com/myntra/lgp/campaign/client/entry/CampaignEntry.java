package com.myntra.lgp.campaign.client.entry;

import com.myntra.commons.entries.BaseEntry;

public class CampaignEntry extends BaseEntry {
	
	private String name;
	
	private String campaignId;
	
	private String description;
	
	private Long startTime;
	
	private Long endTime;
	
	private Long budget;
	
	private String accountName;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Long getBudget() {
		return budget;
	}

	public void setBudget(Long budget) {
		this.budget = budget;
	}
	
	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountInfo) {
		this.accountName = accountInfo;
	}
	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	@Override
	public String toString() {
		return "CampaignEntry [name=" + name + ", campaignId=" + campaignId + ", description=" + description
				+ ", startTime=" + startTime + ", endTime=" + endTime + ", budget=" + budget + ", accountName="
				+ accountName + ", version=" + version + "]";
	}
	
	
	

}
