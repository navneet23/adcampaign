package com.myntra.lgp.campaign.client.entry;

import com.myntra.commons.entries.BaseEntry;


public class AdvertiserEntry extends BaseEntry {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8379445437172860213L;

	private String name;
	
	private String advertiserRefId;
	
	private String description;
	
	public String getAdvertiserRefId() {
		return advertiserRefId;
	}

	public void setAdvertiserRefId(String advertiserId) {
		this.advertiserRefId = advertiserId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String entry) {
		this.description = entry;
	}
	
	@Override
	public String toString() {
		return "AdvertiserEntry [name=" + name + ", advertiserId=" + advertiserRefId + ", description=" + description + "]";
	}

}
