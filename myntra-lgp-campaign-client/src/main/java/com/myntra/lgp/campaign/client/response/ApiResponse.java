package com.myntra.lgp.campaign.client.response;


public class ApiResponse {
	
	int statusCode;
	Object data;
	public ApiResponse(Object data, int statusCode) {
		super();
		this.data = data;
		this.statusCode = statusCode;
	}
	
	public int getStatusCode() {
		return statusCode;
	}
	
	public Object getData() {
		return data;
	}
	

}