package com.myntra.lgp.campaign.client.entry;

import com.myntra.commons.entries.BaseEntry;

public class AccountEntry extends BaseEntry {

	private String name;
	
	private String type;
	
	private Long balance;
	
	// TODO hack for omitting dozer mapping to entity
	private String advertiserRefId;
	
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getBalance() {
		return balance;
	}

	public void setBalance(Long balance) {
		this.balance = balance;
	}

	public String getAdvertiserRefId() {
		return advertiserRefId;
	}

	public void setAdvertiserRefId(String advertiserInfo) {
		this.advertiserRefId = advertiserInfo;
	}

	@Override
	public String toString() {
		return "AccountEntry [name=" + name + ", type=" + type + ", balance="
				+ balance + ", advertiserInfo=" + advertiserRefId + ", getId()="
				+ getId() + ", getCreatedOn()=" + getCreatedOn()
				+ ", getLastModifiedOn()=" + getLastModifiedOn()
				+ ", getCreatedBy()=" + getCreatedBy() + ", getVersion()="
				+ getVersion() + "]";
	}
	
	
	

}
