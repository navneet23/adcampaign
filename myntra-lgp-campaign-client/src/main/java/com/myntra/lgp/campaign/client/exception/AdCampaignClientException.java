package com.myntra.lgp.campaign.client.exception;

public class AdCampaignClientException extends RuntimeException{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public AdCampaignClientException(Exception e) {
		super(e);
	}

	public AdCampaignClientException(String e) {
		super(e);
	}
	
	public AdCampaignClientException(String msg, Exception e) {
		super(msg,e);
	}

}
