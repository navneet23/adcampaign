CREATE DATABASE  IF NOT EXISTS `adExp1`;
USE `adExp1`;

--
-- Table structure for table `book_detail`
--

DROP TABLE IF EXISTS `advertisor`;
CREATE TABLE `advertisor` (
  `id` Bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` TEXT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `advertisor_account`;
CREATE TABLE `advertisor_account` (
  `id` Bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_type` varchar(255) NOT NULL,
  `balance` double unsigned NOT NULL,
  `advertisor_id` Bigint(11) unsigned NOT NULL ,
  PRIMARY KEY (`id`),
  KEY `fk_advertisor_id` (`advertisor_id`),
  CONSTRAINT `fk_advertisor_id` FOREIGN KEY (`advertisor_id`) REFERENCES `advertisor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;